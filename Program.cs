﻿// See https://aka.ms/new-console-template for more information

namespace RPG { 



    public class Character
    {
        public string Name;
        public int Age;
        public int Strength;
        public int Agility;
        public int Intelligence;
        public int Damage;
        public int Health;
        public int Dodge;
        Random rnd = new Random();
        public Character(string name, int age, int str , int agi, int inte)
        {
            Name = name;
            Age = age;
            Strength = str;
            Agility = agi;
            Intelligence = inte;
            Damage = str * 1 + agi * 2 + inte * 1;
            Health = rnd.Next(5,12) * str + 50;
            Dodge = agi * 5;


        }
    }

    public class Enemy
    {
        public string Name;
        public int Health;
        public int Damage;
        Random rnd = new Random(); 
        public Enemy(string name)
        {
            Name =name;
            Health = rnd.Next(20,40);
            Damage = rnd.Next(5,15);
        }
    }

class main
{
        static void Main(string[] args)
        {
            Console.WriteLine("Create hero for your adventure!");
            Console.WriteLine("Press 1 to select Warrior");
            Console.WriteLine("Press 2 to select Mage");
            Console.WriteLine("Press 3 to select Rogue");

            String choise = Console.ReadLine();
            bool correctChoice = false;
            Character Char = new Character("Leopold", 24, 20, 10, 2);


            while (!correctChoice) {

                switch (choise)
                {
                    case "1":
                        Console.WriteLine("You chose: warrior");
                        Char = new Character("Leopold", 24, 20, 5, 2);
                        correctChoice = true;

                        break;
                    case "2":
                        Console.WriteLine("You chose: Mage");
                        Char = new Character("Smarty", 175, 5, 0, 30);
                        correctChoice = true;
                        break;
                    case "3":
                        Console.WriteLine("You chose: Rogue");
                        Char = new Character("Stabby", 6, 5, 20, 5);

                        correctChoice = true; break;
                    default:
                        Console.WriteLine("Please enter a valid value.");
                        Console.WriteLine("Press 1 to select Warrior");
                        Console.WriteLine("Press 2 to select Mage");
                        Console.WriteLine("Press 3 to select Rogue");
                        break;
                }
                if (!correctChoice)
                {
                    choise = Console.ReadLine();
                }
                Console.WriteLine("Name:" + Char.Name + " Age:" + Char.Age + " Damage:" + Char.Damage + " Health:" + Char.Health + " Dodge:" + Char.Dodge);

            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Battle?");
            Console.WriteLine("1 to enter, 2 to flee");
            Console.ForegroundColor = ConsoleColor.White;
            String battle = Console.ReadLine();
            Random rnd = new Random();
            bool Battling = false;
            string[] EnemyNames = new string[] { "Toadster", "Flapster", "Rockster", "A CHILD", "Fishter", "Wormster", "Peasant", "Mosquito-man" };
            int index = rnd.Next(EnemyNames.Length);
            Enemy EnemyToFight = new Enemy(EnemyNames[index]);
            int battles = 0;
            int dodged = rnd.Next(0, 500);
            Console.ForegroundColor = ConsoleColor.White;

            while (!Battling) { 
            switch (battle)
            {
                case "1":
                        if(Char.Health >= 0) {
                            Console.ForegroundColor = ConsoleColor.Magenta;
                            Console.WriteLine("Enemy");
                    Console.WriteLine("Name: "+EnemyToFight.Name +" Damage:"+ EnemyToFight.Damage + " Health:"+ EnemyToFight.Health);
                           dodged = rnd.Next(0, 500);
                            if (Char.Dodge < dodged) { 
                     Char.Health = Char.Health - EnemyToFight.Damage;
                            }
                            else
                            {
                                Char.Health = Char.Health;
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("Attack dodged");
                            }
                            Console.ForegroundColor = ConsoleColor.White;

                            if ((EnemyToFight.Health - Char.Damage) <= 0)
                    {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Enemy Dead");
                            index = rnd.Next(EnemyNames.Length);
                            EnemyToFight = new Enemy(EnemyNames[index]);
                        }
                    else {
                                Console.ForegroundColor = ConsoleColor.White;
                                EnemyToFight.Health -= Char.Damage;
                        Console.WriteLine("Enemy health: "+EnemyToFight.Health);
                    }
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine("Your health is now: "+Char.Health);
                            battles++;
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine("Sorry, you died to " + EnemyToFight.Name);
                            Battling = true;
;                        }
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                case "2":
                        Console.WriteLine("Fleeing");
                        Battling = true;
                       
                    break;
            }
                if (!Battling && Char.Health >= 0)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Continue battling?");
                    Console.WriteLine("1 to enter, 2 to flee");
                    battle = Console.ReadLine();
                    Console.ForegroundColor = ConsoleColor.White;
                } else
                {
                    Battling = true;
                }

            }
            Console.WriteLine("Game is done, you completed " + battles + " battles!");
            if (battles >= 0 && battles < 10)
            {
                Console.WriteLine("Thats a puny amount of battles, you can do better!");
            }
            else if (battles >= 10 && battles < 20)
            {
                Console.WriteLine("Looking strong hero!");
            }
            else if(battles >= 20){
                Console.WriteLine("You are a legendary fighter!");
            }
        }
}
    }